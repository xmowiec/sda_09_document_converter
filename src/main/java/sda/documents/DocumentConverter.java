package sda.documents;

import sda.documents.readers.FileReaderFactory;
import sda.documents.readers.IFileReader;
import sda.documents.exceptions.FileReaderException;
import sda.documents.exceptions.FileWriterException;
import sda.documents.writers.FileWriterFactory;
import sda.documents.writers.IFileWriter;
import sda.documents.writers.IFileWriterFacory;

import java.util.List;
import java.util.Map;

public class DocumentConverter implements IDocumentConverter {
    @Override
    public void convert(String inputFilePath, String outputFilePath) throws FileReaderException, FileWriterException {
        FileReaderFactory fileReaderFactory = new FileReaderFactory();
        IFileReader reader = fileReaderFactory.produce(inputFilePath);
        List<Map<String, String>> data = reader.read(inputFilePath);

        IFileWriterFacory fileWriterFactory = new FileWriterFactory();
        IFileWriter writer = fileWriterFactory.produce(outputFilePath);
        writer.write(outputFilePath,data);
    }
}
