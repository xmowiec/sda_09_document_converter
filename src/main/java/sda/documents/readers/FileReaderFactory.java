package sda.documents.readers;

import java.util.UnknownFormatFlagsException;

public class FileReaderFactory implements IFileReaderFactory {

    private static final String DOT_REGEX = "\\.";

    @Override
    public IFileReader produce(String filePath) {
        String[] split = filePath.split(DOT_REGEX);
        String extension = split[split.length - 1];
        IFileReader reader;

        switch (extension) {
            case "csv":
                reader = new CsvReader();
                break;
            case "xml":
                reader = new XmlReader();
                break;
            default:
                throw new UnknownFormatFlagsException("Nieznany format pliku");
        }

        return reader;
    }
}
