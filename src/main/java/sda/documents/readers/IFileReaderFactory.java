package sda.documents.readers;

public interface IFileReaderFactory {
    IFileReader produce(String filePath);
}
