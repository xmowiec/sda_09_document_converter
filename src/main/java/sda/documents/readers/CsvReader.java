package sda.documents.readers;

import sda.documents.exceptions.FileReaderException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class CsvReader implements IFileReader {
    private static final String CSV_SEPARATOR = ",";

    @Override
    public List<Map<String, String>> read(String filePath) throws FileReaderException {
        List<String> lines;
        try {
            lines = readRecords(filePath);
        } catch (IOException e) {
            throw new FileReaderException(e.getMessage(), e.getCause());
        }
        List<Map<String, String>> records = new ArrayList<>();

        // read first line of CSV file - names of fields
        String[] fields = lines.get(0).split(CSV_SEPARATOR);
        for (int i = 1; i < lines.size(); i++) {
            // LinkedHashMap keeps order of data
            Map<String, String> record = new LinkedHashMap<>();
            String[] line = lines.get(i).split(CSV_SEPARATOR);
            for (int j = 0; j < fields.length; j++) {
                record.put(fields[j], line[j]); // put into map
            }
            records.add(record); // add into list
        }
        return records;
    }

    private List<String> readRecords(String filePath) throws IOException {
        return Files.readAllLines(Paths.get(filePath));
    }
}
