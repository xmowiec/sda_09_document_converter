package sda.documents;

import sda.documents.exceptions.FileReaderException;
import sda.documents.exceptions.FileWriterException;

public class App {
    public static void main(String[] args) {
        String inputFilePath = "c:\\Users\\User\\IdeaProjects\\sda_j5_documentconverter\\" +
                "src\\main\\resources\\input.csv";
        String outputFilePath = "c:\\Users\\User\\IdeaProjects\\sda_j5_documentconverter\\" +
                "src\\main\\resources\\output.csv";
        IDocumentConverter documentConverter = new DocumentConverter();
        try {
            documentConverter.convert(inputFilePath, outputFilePath);
        } catch (FileReaderException e) {
            System.out.println("Nieznany format pliku wejściowego.");
        } catch (FileWriterException e) {
            System.out.println("Nieznany format pliku wyjściowego.");
        }
    }
}
