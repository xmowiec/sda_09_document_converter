package sda.documents.writers;

import sda.documents.exceptions.FileWriterException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CsvWriter implements IFileWriter {

    private static final String CSV_SEPARATOR = ",";

    @Override
    public void write(String filePath, List<Map<String, String>> data) throws FileWriterException {

        try {
            File file = new File(filePath);
            FileOutputStream outputStream = new FileOutputStream(file);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            List<String> headers = new ArrayList<>();

            headers.addAll(data.get(0).keySet());

            StringBuilder buildLine = new StringBuilder();
            for (String header : headers) {
                buildLine.append(header).append(CSV_SEPARATOR);
            }
            buildLine.deleteCharAt(buildLine.length()-1);
            buildLine.append("\r\n");
            outputStream.write(buildLine.toString().getBytes());

            for (Map<String, String> record : data) {
                StringBuilder buildDataLine = new StringBuilder();
                for (String header : headers) {
                    buildDataLine.append(record.get(header)).append(CSV_SEPARATOR);
                }
                buildDataLine.deleteCharAt(buildDataLine.length()-1);
                buildDataLine.append("\r\n");
                outputStream.write(buildDataLine.toString().getBytes());
            }
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            throw new FileWriterException(e.getMessage(), e);
        }

    }
}
