package sda.documents.writers;

import sda.documents.exceptions.FileWriterException;

public class FileWriterFactory implements IFileWriterFacory {

    private static final String DOT_REGEX = "\\.";

    @Override
    public IFileWriter produce(String filePath) throws FileWriterException {

        String[] split = filePath.split(DOT_REGEX);
        String extension = split[split.length - 1];
        IFileWriter writer;

        switch (extension) {
            case "csv":
                writer = new CsvWriter();
                break;
            case "xml":
                writer = new XmlWriter();
                break;
            default:
                throw new FileWriterException("Nieznany format pliku");
        }

        return writer;
    }
}

