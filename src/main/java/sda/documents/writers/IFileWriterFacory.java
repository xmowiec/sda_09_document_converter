package sda.documents.writers;

import sda.documents.exceptions.FileWriterException;

public interface IFileWriterFacory {
    IFileWriter produce(String filePath) throws FileWriterException;
}
