package sda.documents.exceptions;

import java.io.IOException;

public class FileWriterException extends Exception {
    public FileWriterException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileWriterException(String message) {
        super(message);
    }
}
